# CoffeeConf
CoffeeConf is a pleasant library for CoffeeScript and JavaScript running on Node.js. It provides tools for configuration. Note that here I use the JavaScript syntax. 
# Contributing
Feel free to make merge requests, issues, whatever your heart pleases! Note; don't edit package.json. `package.yaml` is autocompiled to `package.json`, so please use that (I like YAML too much, don't I? 😩).
## CoffeeConf - class
Note; instead of using the `new` keyword to use the constructor, you can do `CoffeeConf.access(arg);`. This throws an error if the database doesn't exist; useful for checking if it does (with the `try..catch` construct). Also, use `path.join()` to join paths; works 100%. Note you gotta require path first. JS: `const path = require('path');`. CoffeeScript: `path = require 'path'`

```js
mkcfg('cfgname', 'content', {as_json: false, as_yaml: false, prepend_newline: false, append_newline: false})
```
Creates a new config file `cfgname` with content `content`. The content defaults to an empty string.
If file exists, it is overwritten
Options in the options object literal are self explanatory.
```js
mknested('nesteddirname')
```
This creates a nested directory `nesteddirname` inside your database.

```js
readcfg('filename', {as_json: false, as_yaml: false, prepend_newline: false, append_newline: false})
```
Reads a config file `filename`.
Options in the options object literal are self explanatory.

```js
rmcfg('filename')
```
Removes the configuration file `filename` in your database.
```js
rmdb()
```
Simply removes the whole database that this is called on. 

```js
rmnested('nesteddirname')
```
Removes a nested directory `nesteddirname` inside your database.

```js
writecfg('cfgname', 'content', {as_json: false, as_yaml: false, prepend_newline: true, append_newline: false})
```
Creates a new config file `cfgname` with content `content`. The content defaults to an empty string.
If file already exists, it is appended to. Please use mkcfg to make new files.

Options in the options object literal are self explanatory.

```js
exist('filename')
```
Returns a boolean (true/false) based on if the file `filename` exists in your database or not.
